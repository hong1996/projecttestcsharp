﻿using System;
using System.IO;
using System.Linq;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Program program = new Program();
            string pathInput = "";
            string pathOutput = "";

            String inputFile = "";
            String outputFile = "";

            int row = 0;
            int col = 0;

            int rowOut = 0;
            int colOut = 0;

            if (args.Any() && args.Length > 1)
            {
                pathInput = args[0];
                pathOutput = args[1];
            }

            if (File.Exists(pathInput))
            {
                inputFile = File.ReadAllText(pathInput);
                row = inputFile.Split('\n').Length;
                if (row > 0)
                {
                    string oneRow = inputFile.Split('\n')[0];
                    col = oneRow.Trim().Split(' ').Length;
                }
            }
            else
            {
                Console.Write("File input khong ton tai");
                return;
            }

            if (File.Exists(pathOutput))
            {
                outputFile = File.ReadAllText(pathOutput);
                rowOut = outputFile.Split('\n').Length;
                if (rowOut > 0)
                {
                    string oneRow = outputFile.Split('\n')[0];
                    colOut = oneRow.Trim().Split(' ').Length;
                }
            }
            else
            {
                Console.Write("File output khong ton tai");
                return;
            }

            if (row != rowOut || col != colOut)
            {
                Console.Write("FALSE");
                return;
            }

            int[,] input = program.convertDataFileToArray(inputFile, row, col);
            if (input == null)
            {
                return;
            }

            int[,] output = program.convertDataFileToArray(outputFile, rowOut, colOut);
            if (output == null)
            {
                return;
            }

            int[,] outputCheck = program.renderOutputFromInput(input, row, col);

            for (int i = 0; i < row; i++)
            {
                for (int j = 0; j < col; j++)
                {
                    if (output[i, j] != outputCheck[i, j])
                    {
                        Console.Write("FALSE");
                        return;
                    }
                }
            }

            Console.Write("TRUE");
            return;
        }

        int[,] convertDataFileToArray(string inputFile, int row, int col)
        {
            int[,] output = new int[row, col];
            if (row < 2 || row > 100)
            {
                Console.Write("So dong can thoa man dieu kien la: M >= 2 và M <= 100");
                return null;
            }

            if (col < 2 || col > 100)
            {
                Console.Write("So cot can thoa man dieu kien la: N >= 2 và N <= 100");
                return null;
            }
            int ii = 0, jj = 0; int temp = 0;
            foreach (var rowItem in inputFile.Split('\n'))
            {
                jj = 0;
                foreach (var colItem in rowItem.Trim().Split(' '))
                {
                    if (int.TryParse(colItem.Trim(), out temp))
                    {
                        output[ii, jj] = int.Parse(colItem.Trim());
                    }
                    jj++;
                }
                ii++;
            }
            return output;
        }

        int[,] renderOutputFromInput(int[,] input, int row, int col)
        {
            int[,] output = new int[row, col];
            for (int i = 0; i < row; i++)
            {
                for (int j = 0; j < col; j++)
                {
                    if (input[i, j] == 1)
                    {
                        if (i - 1 >= 0)
                        {
                            output[i - 1, j] += 1;
                        }
                        if (j - 1 >= 0)
                        {
                            output[i, j - 1] += 1;
                        }
                        if (i - 1 >= 0 && j - 1 >= 0)
                        {
                            output[i - 1, j - 1] += 1;
                        }
                        if (i + 1 < row)
                        {
                            output[i + 1, j] += 1;
                        }
                        if (j + 1 < col)
                        {
                            output[i, j + 1] += 1;
                        }
                        if (i + 1 < row && j + 1 < col)
                        {
                            output[i + 1, j + 1] += 1;
                        }
                        if (i + 1 < row && j - 1 >= 0)
                        {
                            output[i + 1, j - 1] += 1;
                        }
                        if (i - 1 >= 0 && j + 1 < col)
                        {
                            output[i - 1, j + 1] += 1;
                        }
                    }
                }
            }
            return output;
        }
    }
}
